# Kestra.jl

Julia implementation of a similar [Python package Kestra](https://pypi.org/project/kestra/), which serves to ease working with the data orchestrator [Kestra](https://kestra.io).

## Usage

The usage can be nicely demonstrated on an example flow:
```yaml
id: kestra-jl
namespace: tests

vars:
  some_variable: 1

tasks:
  - id: example-jl
    type: io.kestra.plugin.scripts.julia.Script
    runner: PROCESS
    beforeCommands:
      - julia -e 'using Pkg; Pkg.add("Kestra")'
    script: |
      using Kestra

      variable = @from_kestra Int {{vars.some_variable}}
      # Alternatively, if parse(<type>, value) is defined
      # variable = @from_kestra Int "{{vars.some_variable}}"
      outputs(Dict('variable' => variable))
```

> **Important**: As of now, the package is not registered yet -- though it is a work in progress and it should happen in a couple of days.

## License & Contributions

This package is published under the MIT license. Any contributions are welcome, whether via raising Issues or Merge requests.