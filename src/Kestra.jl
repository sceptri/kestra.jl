module Kestra

import HTTP, JSON

export KestraInstance, FlowExecution
export check_status, get_logs, execute
export outputs, counter, timer
# These are NOT in the original Python package
export file_outputs, file_read, @from_kestra

module Internal
using JSON, Dates

send(hashmap) = print("::" * JSON.json(hashmap) * "::")

function metrics(name, type, value; tags=nothing)
    send(Dict(
        "metrics" => [
            Dict(
                "name" => name,
                "type" => type,
                "value" => value,
                "tags" => something(tags, [])
            )
        ]
    ))
end
end

outputs(hashmap) = Internal.send(Dict("outputs" => hashmap))

file_outputs(write_to, object) = write(write_to, JSON.json(object))
function file_read(file_to_read; to_json = false)
	contents = read(file_to_read, String)

	return to_json ? JSON.parse(contents) : contents
end

macro from_kestra(type, value)
	type = eval(type)
	if type <: AbstractString
		return string(value)
	elseif type <: AbstractDict
		return JSON.parse(string(value))
	else
		try
			return parse(type, value)
		catch
			return value
		end
	end
end

counter(name, value; tags=nothing) = Internal.metrics(name, "counter", value; tags)

function timer(name, duration; tags=nothing)
    if typeof(duration) <: Base.Callable
        start_time = now()
        duration()
        Internal.metrics(
            name,
            "timer",
            (now() - start_time).value / 1000;
            tags
        )
    else
        Internal.metrics(name, "timer", duration; tags)
    end
end

const API_ENDPOINT_EXECUTION_CREATE = "/api/v1/executions/trigger/PARAM_FLOW_ID"
const API_ENDPOINT_EXECUTION_STATUS = "/api/v1/executions/PARAM_EXECUTION_ID"
const API_ENDPOINT_EXECUTION_LOG = "/api/v1/logs/PARAM_EXECUTION_ID/download"

StringOrNone = Union{AbstractString,Nothing}

Base.@kwdef struct KestraInstance
    wait_for_completion::Bool = true
    poll_interval::Int = 1
    labels_from_inputs::Bool = false
    user::StringOrNone = get(ENV, "KESTRA_USER", nothing)
    password::StringOrNone = get(ENV, "KESTRA_PASSWORD", nothing)
    hostname::AbstractString = get(ENV, "KESTRA_HOSTNAME", "http://localhost:4041")
end

Base.@kwdef mutable struct FlowExecution
    status::StringOrNone = nothing
    log::StringOrNone = nothing
    error::StringOrNone = nothing
end

const Instance = KestraInstance

function make_request(kestra::KestraInstance, method::AbstractString, url::AbstractString; response_stream=IOBuffer(), kwargs...)
    if !isnothing(kestra.user) && !isnothing(kestra.password)
        url = replace(r"://" => "://" * kestra.user * ":" * kestra.password)
    end

    try
        response = HTTP.request(uppercase(method), url; status_exception=true, response_stream, kwargs...)

        return response
    catch exception
        if exception.status == 401
            error("Authentication required but not provided. Please set the username and password.")
        else
            error(exception)
        end
    end

    return nothing
end

function check_status(kestra::KestraInstance, execution_id::String; kwargs...)
    return make_request(kestra, "get", replace(
        kestra.hostname * API_ENDPOINT_EXECUTION_STATUS,
        "PARAM_EXECUTION_ID" => execution_id
    ); kwargs...)
end

function get_logs(kestra::KestraInstance, execution_id::String; kwargs...)
    return make_request(kestra, "get", replace(
        kestra.hostname * API_ENDPOINT_EXECUTION_LOG,
        "PARAM_EXECUTION_ID" => execution_id
    ); kwargs...)
end

parse_body(response::HTTP.Response) = String(take!(response.body))
json_body(response::HTTP.Response) = JSON.parse(parse_body(response))

function execute(kestra::KestraInstance, namespace::String, flow::String, inputs::Dict=Dict(); kwargs...)
    @debug ("Starting a flow $flow in the namespace $namespace with parameters " * JSON.json(inputs))

    result = FlowExecution()
    url_default = kestra.hostname * replace(API_ENDPOINT_EXECUTION_CREATE,
        "PARAM_FLOW_ID" => "$namespace/$flow"
    )

    if kestra.labels_from_inputs && length(inputs) > 0
        labels = "?" * join(["$key:$value" for (key, value) in inputs], '&')
        url = url_default * labels

        response = make_request(kestra, "post", url; body=HTTP.Form(inputs))
    elseif length(inputs) > 0
        response = make_request(kestra, "post", url_default; body=HTTP.Form(inputs))
    else
        response = make_request(kestra, "post", url_default)
    end

    response_body = json_body(response)

    if !haskey(response_body, "id")
        error("tarting execution failed: " * string(response) * string(response_body))
    end

    execution_id = response_body["id"]
    @info "Successfully triggered the execution: $(kestra.hostname)/ui/executions/$namespace/$flow/$execution_id"

    if kestra.wait_for_completion
        finished = false
        while !finished
            response = check_status(kestra, execution_id) |> json_body
            log = get_logs(kestra, execution_id) |> parse_body

            if "SUCCESS" == response["state"]["current"]
                @info "Execution of the flow $flow in the namespace $namespace with parameters $(JSON.json(inputs)) was successful \n$log"

                result.status = response["state"]["current"]
                result.log = string(log)
                finished = true
            elseif "WARNING" == response["state"]["current"]
                @info "Execution of the flow $flow in the namespace $namespace with parameters $(JSON.json(inputs)) finished with warnings \n$log"

                result.status = response["state"]["current"]
                result.log = string(log)
                finished = true
            elseif "FAILED" == response["state"]["current"]
                @info "Execution of the flow $flow in the namespace $namespace with parameters $(JSON.json(inputs)) failed \n$log"

                result.status = response["state"]["current"]
                result.log = string(log)
                finished = true
            elseif "KILLED" == response["state"]["current"]
                @info "Execution of the flow $flow in the namespace $namespace with parameters $(JSON.json(inputs)) has been killed \n$log"

                result.status = response["state"]["current"]
                result.log = string(log)
                finished = true
            end

            sleep(kestra.poll_interval)
        end
    else
        result.status = "STARTED"
    end

    return result
end

end # module Kestra